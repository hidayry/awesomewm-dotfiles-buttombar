---@diagnostic disable: undefined-global
local wibox = require 'wibox'
local beautiful = require 'beautiful'
local awful = require 'awful'
local helpers = require 'helpers'
local app = require 'configuration.app'
--local dpi = beautiful.xresources.apply_dpi

local clip = wibox.widget({
		widget = wibox.widget.textbox,
		markup = helpers.get_colorized_markup("󰅎", beautiful.blue),
		font = beautiful.material_icons .. " 13",
		align = "center",
	})


local tooltip = helpers.make_popup_tooltip('Clipboard', function (d)
    return awful.placement.bottom_right(d, {
        margins = {
            bottom = beautiful.bar_height + beautiful.useless_gap * 2,
            right = beautiful.useless_gap * 2 + 200,
        }
    })
end)

tooltip.attach_to_object(clip)

clip:add_button(awful.button({}, 1, function ()
        awful.spawn(app.clip,false)
    end))

return clip
