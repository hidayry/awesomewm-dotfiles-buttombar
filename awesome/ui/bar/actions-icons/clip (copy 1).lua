---@diagnostic disable: undefined-global
local wibox = require 'wibox'
local beautiful = require 'beautiful'
local awful = require 'awful'
local helpers = require 'helpers'
local dpi = beautiful.xresources.apply_dpi

local clip = wibox.widget {
    widget = wibox.widget.imagebox,
    forced_height = dpi(18),
    forced_width = dpi(18),
    image = beautiful.clip_icon,
    halign = 'center',
    valign = 'bottom',
}

local tooltip = helpers.make_popup_tooltip('cipboard', function (d)
    return awful.placement.bottom_right(d, {
        margins = {
            bottom = beautiful.bar_height + beautiful.useless_gap * 2,
            right = beautiful.useless_gap * 2 + 190,
        }
    })
end)

tooltip.attach_to_object(clip)

clip:add_button(awful.button({}, 1, function ()
        awful.spawn(require("startup").clipcmd,false)
    end))

return clip
