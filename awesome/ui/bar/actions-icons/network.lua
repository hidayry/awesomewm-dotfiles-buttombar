---@diagnostic disable: undefined-global
local wibox = require 'wibox'
local beautiful = require 'beautiful'
--local gfs = require 'gears.filesystem'
local awful = require 'awful'
local helpers = require 'helpers'
local app = require "configuration.app"
require "signal.network"

local nme = {
    ssid = "/sbin/iwgetid -r",
}

local network = wibox.widget {
    widget = wibox.widget.textbox,
    font = beautiful.material_icons .. ' 12',
    align = 'center',
    markup = helpers.get_colorized_markup("󰤨", beautiful.blue),
}

local tooltip = helpers.make_popup_tooltip('Press to toggle network', function (d)
    return awful.placement.bottom_right(d, {
        margins = {
            bottom = beautiful.bar_height + beautiful.useless_gap * 2,
            right = beautiful.useless_gap * 2 + 140,
        }
    })
end)

tooltip.attach_to_object(network)

network:add_button(awful.button({}, 1, function ()
    awful.spawn(app.network, false)
end))

awesome.connect_signal('network::connected', function (is_connected)
    network.markup = is_connected
        and helpers.get_colorized_markup("󰤨", beautiful.blue)
        or helpers.get_colorized_markup("󰤭", beautiful.red)

end)

return network
