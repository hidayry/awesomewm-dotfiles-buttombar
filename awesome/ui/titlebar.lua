---@diagnostic disable: undefined-global
local awful = require("awful")
local wibox = require("wibox")
local beautiful = require("beautiful")
local xresources = require("beautiful.xresources")
local dpi = xresources.apply_dpi

client.connect_signal("request::titlebars", function(c)
	local titlebar = awful.titlebar(c, { position = "top", size = 30 })

	local titlebars_buttons = {
		awful.button({}, 1, function()
			c:activate({ context = "titlebar", action = "mouse_move" })
		end), awful.button({}, 3, function()
		c:activate({ context = "titlebar", action = "mouse_resize" })
	end)
	}

	local buttons_loader = {
		layout = wibox.layout.fixed.horizontal,
		buttons = titlebars_buttons
	}

	local function paddined_button(button, margins)
		margins = margins or { top = 7, bottom = 7, left = 5, right = 5 }

		return wibox.widget({
			button,
			top = margins.top,
			bottom = margins.bottom,
			left = margins.left,
			right = margins.right,
			widget = wibox.container.margin
		})
	end

	titlebar:setup({

		{     -- left
			--paddined_button(awful.titlebar.widget.ontopbutton(c)),
			--paddined_button(awful.titlebar.widget.stickybutton(c)),
			-- paddined_button(awful.titlebar.widget.floatingbutton(c)),
			layout = wibox.layout.fixed.horizontal
		},
		-- { -- Middle
		-- {
		-- align = "center",
		-- font = beautiful.font_name .. "16",
		-- widget = awful.titlebar.widget.titlewidget(c),
		-- },
		-- layout = wibox.layout.flex.horizontal,
		-- },
		{     -- Middle
			layout = wibox.layout.fixed.horizontal
		},
		{     -- right
			paddined_button(awful.titlebar.widget.ontopbutton(c)),
			paddined_button(awful.titlebar.widget.stickybutton(c)),
			paddined_button(awful.titlebar.widget.minimizebutton(c)),
			paddined_button(awful.titlebar.widget.maximizedbutton(c)),
			paddined_button(awful.titlebar.widget.closebutton(c)),
			layout = wibox.layout.fixed.horizontal
		},
		buttons_loader,
		buttons_loader,
		layout = wibox.layout.align.horizontal
	})
end)
