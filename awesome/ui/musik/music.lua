-- music notification
-- ~~~~~~~~~~~~~~~~~~
-- requirements
-- ~~~~~~~~~~~~
local naughty = require("naughty")
local playerctl = require("bling").signal.playerctl.lib()
local beautiful = require("beautiful")

-- misc/vars
-- ~~~~~~~~~

local notif

-- buttons
local previous_button = naughty.action {name = "previous"}
local next_button = naughty.action {name = "next"}

-- actual buttons
previous_button:connect_signal('invoked', function() playerctl:previous() end)

next_button:connect_signal('invoked', function() playerctl:next() end)

-- init
-- ~~~~

-- notification
playerctl:connect_signal("metadata",
                         function(_, title, artist, album_path, __, new, ___)

    -- destroy notif when not needed
    -- send notification when a new song is played
    if new then
        naughty.notify({
            app_name = "Music",
            title = title or "Song",
            text = "~ " .. artist,
            actions = {previous_button, next_button},
            image = album_path
        }, notif)
    end

end)
