-- titlebar decorations for ncmpcpp
-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

-- requirements
-- ~~~~~~~~~~~~
local awful = require("awful")
local helpers = require("helpers")
local wibox = require("wibox")
local beautiful = require("beautiful")
local gears = require("gears")
local ruled = require("ruled")
local dpi = beautiful.xresources.apply_dpi


-- widgets
-- ~~~~~~~

-- album art
local album_art = wibox.widget{
    widget = wibox.widget.imagebox,
    clip_shape = helpers.rrect(beautiful.rounded),
    forced_height = dpi(60),
    forced_width = dpi(60),
    halign = "center",
    valign = "center",
    image = beautiful.fallback_music
}

-- song artist
local song_artist = wibox.widget{
    widget = wibox.widget.textbox,
    markup = helpers.get_colorized_markup("Unknown", beautiful.fg_focus or beautiful.fg_normal),
    font = beautiful.font_name .. "10",
    align = "center",
    valign = "bottom"
}

-- song name
local song_name = wibox.widget{
    widget = wibox.widget.textbox,
    markup = helpers.get_colorized_markup("None", beautiful.fg_focus or beautiful.fg_normal),
    font = beautiful.font_name .. "Bold 11",
    align = "center",
    valign = "bottom"
}



-- buttons --

-- toggle button
local toggle_button = wibox.widget{
    widget = wibox.widget.textbox,
    markup = helpers.get_colorized_markup("", beautiful.fg_normal),
    font = beautiful.nerd_font .. " 19",
    align = "center",
    valign = "center"
}

-- next button
local next_button = wibox.widget{
    widget = wibox.widget.textbox,
    markup = helpers.get_colorized_markup("", beautiful.fg_focus or beautiful.fg_normal .. "99"),
    font = beautiful.nerd_font .. " 17",
    align = "center",
    valign = "center"
}

-- prev button
local prev_button = wibox.widget{
    widget = wibox.widget.textbox,
    markup = helpers.get_colorized_markup("", beautiful.fg_focus or beautiful.fg_normal .. "99"),
    font = beautiful.nerd_font .. " 17",
    align = "center",
    valign = "center"
}



-- progressbar
local pbar = wibox.widget {
    widget = wibox.widget.progressbar,
    forced_height = dpi(4),
    color = beautiful.blue,
    background_color = beautiful.blue .. "4D",
    value = 50,
    max_value = 100,
    halign = "left",
    valign = "center",
}










-- update widgets
-- ~~~~~~~~~~~~~~
local playerctl = require("bling").signal.playerctl.lib()

local toggle_command = function() playerctl:play_pause() end
local prev_command = function() playerctl:previous() end
local next_command = function() playerctl:next() end

toggle_button:buttons(gears.table.join(
    awful.button({}, 1, function() toggle_command() end)))

next_button:buttons(gears.table.join(
    awful.button({}, 1, function() next_command() end)))

prev_button:buttons(gears.table.join(
    awful.button({}, 1, function() prev_command() end)))


playerctl:connect_signal("metadata", function(_, title, artist, album_path, _, __, ___)
	if title == "" then
		title = "None"
	end
	if artist == "" then
		artist = "Unknown"
	end
	if album_path == "" then
		album_path = beautiful.fallback_music
	end


	album_art:set_image(gears.surface.load_uncached(album_path))
    song_name:set_markup_silently(helpers.get_colorized_markup(title, beautiful.fg_focus or beautiful.fg_normal))
	song_artist:set_markup_silently(helpers.get_colorized_markup(artist, beautiful.fg_focus or beautiful.fg_normal))
end)


playerctl:connect_signal("playback_status", function(_, playing, _)
	if playing then
        toggle_button.markup = helpers.get_colorized_markup("", beautiful.fg_normal)
	else
        toggle_button.markup = helpers.get_colorized_markup("", beautiful.fg_normal)
	end
end)

playerctl:connect_signal("position", function(_, current_pos, total_pos, player_name)
    pbar.value = (current_pos / total_pos) * 100

end)






-- effects
--~~~~~~~~

prev_button:connect_signal("mouse::enter", function ()
    prev_button.markup = helpers.get_colorized_markup("", beautiful.blue)
end)
prev_button:connect_signal("mouse::leave", function ()
    prev_button.opacity = 1
    prev_button.markup = helpers.get_colorized_markup("", beautiful.fg_normal .. "99")
end)

prev_button:connect_signal("button::press", function ()
    prev_button.markup = helpers.get_colorized_markup("", beautiful.red)
end)
prev_button:connect_signal("button::release", function ()
    prev_button.markup = helpers.get_colorized_markup("", beautiful.blue)
end)


next_button:connect_signal("mouse::enter", function ()
    next_button.markup = helpers.get_colorized_markup("", beautiful.blue)
end)
next_button:connect_signal("mouse::leave", function ()
    next_button.markup = helpers.get_colorized_markup("", beautiful.fg_normal .. "99")
end)

next_button:connect_signal("button::press", function ()
    next_button.markup = helpers.get_colorized_markup("", beautiful.red)
end)
next_button:connect_signal("button::release", function ()
    next_button.markup = helpers.get_colorized_markup("", beautiful.blue)
end)







local music_init = function (c)

    -- Hide default titlebar
    -- awful.titlebar.hide(c)



        -- bottom
        awful.titlebar(c, { position = "bottom", size = dpi(80), bg = beautiful.bg_lighter or beautiful.bg_contrast }):setup {
            pbar,
            {
                {
                    {
                        album_art,
                        layout = wibox.layout.fixed.horizontal
                    },
                    {
                        nil,
                        {
                            song_name,
                            song_artist,
                            layout = wibox.layout.fixed.vertical,
                            spacing = dpi(2)
                        },
                        expand = "none",
                        layout = wibox.layout.align.vertical
                    },
                    {
                        prev_button,
                        toggle_button,
                        next_button,
                        layout = wibox.layout.fixed.horizontal,
                        spacing = dpi(15)
                    },
                    layout = wibox.layout.align.horizontal,
                    expand = "none"
                },
                margins = dpi(12),
                widget = wibox.container.margin
            },
            layout = wibox.layout.fixed.vertical,
            spacing = dpi(0)
        }




    c.custom_decoration = { top = true, left = true, bottom = true }



end

-- Add the titlebar whenever a new music client is spawned
ruled.client.connect_signal("request::rules", function()
    ruled.client.append_rule {
        id = "music",
        rule = {instance = "music"},
        callback = music_init
    }
end)

