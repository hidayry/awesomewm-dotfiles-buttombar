---@diagnostic disable: undefined-global
local awful = require "awful"
local bling = require("bling")


-- bling layouts
local mstab = bling.layout.mstab
local equal = bling.layout.equalarea

local function set_layouts()
    tag.connect_signal("request::default_layouts", function ()
        awful.layout.append_default_layouts {
            awful.layout.suit.tile,
            awful.layout.suit.floating,
            awful.layout.suit.max,
            awful.layout.suit.tile.bottom,
            mstab,
            equal,
        }
    end)
end

set_layouts()
