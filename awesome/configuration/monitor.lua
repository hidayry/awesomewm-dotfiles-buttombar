local awful = require 'awful'
local bling = require("bling")
local beautiful = require("beautiful")
local gears = require("gears")
require("awful.autofocus")

awful.screen.connect_for_each_screen(function(s)
	s:connect_signal('property::geometry', function() awesome.restart() end)
end)

client.connect_signal("request::manage", function(c)
	--- Add missing icon to client
	if not c.icon then
		local icon = gears.surface(beautiful.theme_assets.awesome_icon(24,
			beautiful.blue,
			beautiful.bg_normal))
		c.icon = icon._native
		icon:finish()
	end

	--- Set the windows at the slave,
	if awesome.startup and not c.size_hints.user_position and
			not c.size_hints.program_position then
		--- Prevent clients from being unreachable after screen count changes.
		awful.placement.no_offscreen(c)
	end
	awful.placement.centered(c, { honor_workarea = true })
end)

--- Hide all windows when a splash is shown
awesome.connect_signal("widgets::splash::visibility", function(vis)
	local t = screen.primary.selected_tag
	if vis then
		for idx, c in ipairs(t:clients()) do c.hidden = true end
	else
		for idx, c in ipairs(t:clients()) do c.hidden = false end
	end
end)

--- Flash focus
bling.module.flash_focus.enable()

--- Enable for lower memory consumption
collectgarbage("setpause", 110)
collectgarbage("setstepmul", 1000)
gears.timer({
	timeout = 5,
	autostart = true,
	call_now = true,
	callback = function() collectgarbage("collect") end
})
