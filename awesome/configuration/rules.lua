local ruled = require("ruled")
local awful = require("awful")

local function setup_rules()
	ruled.client.connect_signal("request::rules", function()
		ruled.client.append_rule({
			id = "global",
			rule = {},
			properties = {
				focus = awful.client.focus.filter,
				raise = true,
				screen = awful.screen.preferred,
				placement = awful.placement.no_overlap + awful.placement.no_offscreen,
			},
		})
		ruled.client.append_rule({
			id = "floating",
			rule_any = {
				instance = { "music", "fetch", "ranger" },
				class = {
					"Pavucontrol",
					"Engrampa",
					"Lxappearance",
					"Nm-connection-editor",
					"Densify",
					"lightdm-gtk-greeter-settings",
					"qt5ct",
					"Timeshift-gtk",
					"GParted",
				},
				name = {
					"Event Tester", -- xev.
				},
				role = {
					"AlarmWindow", -- Thunderbird's calendar.
					"ConfigManager", -- Thunderbird's about:config.
					"pop-up", -- e.g. Google Chrome's (detached) Developer Tools.
				},
			},
			properties = { floating = true },
		})
		ruled.client.append_rule({
			id = "titlebars",
			rule_any = { type = { "normal", "dialog" } },
			properties = { titlebars_enabled = true },
		})
	end)

	ruled.notification.connect_signal("request::rules", function()
		ruled.notification.append_rule({
			rule = {},
			properties = {
				screen = awful.screen.preferred,
				implicit_timeout = 5,
			},
		})
	end)
end

-- Music client
ruled.client.append_rule({
	rule_any = { class = { "music" }, instance = { "music" } },
	properties = {
		floating = true,
		width = 700,
		height = 444,
	},
})

-- infofetch
ruled.client.append_rule({
	rule_any = { class = { "fetch" }, instance = { "fetch" } },
	properties = {
		floating = true,
		width = 700,
		height = 444,
	},
})

-- ranger
ruled.client.append_rule({
	rule_any = { class = { "ranger" }, instance = { "ranger" } },
	properties = {
		floating = true,
		width = 800,
		height = 400,
	},
})

ruled.client.append_rule({
	rule_any = { class = { "mpv" }},
	properties = {
		floating = true,
		width = 1300,
		height = 600,
	},
})


setup_rules()
