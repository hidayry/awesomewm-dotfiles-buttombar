local os = require("os")
local home_var = os.getenv("HOME")

return {
    clip = home_var .. "/.config/awesome/app/clip/clip.sh",
    terminal = "st",
    filemanager = "thunar",
    browser = "firefox",
    launcher = home_var .. "/.config/awesome/app/launcher/launcher.sh",
    visual_editor = "codium",
    editor_cmd = "st -e nvim",
    network = "networkmanager_dmenu",
    music = "alacritty --class 'music' --config-file " .. home_var ..
        "/.config/alacritty/ncmpcpp.yml -e ncmpcpp ",
    userdesc = "@Debian-AwesomeWM",
    username = os.getenv("USER"):gsub("^%l", string.upper),
    spasi = "                                    ",
    fetch = "alacritty --class 'fetch' --config-file " .. home_var ..
        "/.local/bin/sysfetch",
    ranger = "alacritty -e" .. "ranger",
    picker = home_var .. "/.config/awesome/app/colorpicker",
    musicMenu = function() require("startup.Rofi.music-pop").execute() end
}
