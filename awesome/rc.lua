pcall(require, "luarocks.loader")

modkey = "Mod4" -- super, the windows key
require ("awful.autofocus")
require("signal.global")
--require("user_likes")
require("configuration")
require("ui")
require("startup")
